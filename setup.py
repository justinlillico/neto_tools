from setuptools import setup

setup(
    # Needed to silence warnings (and to be a worthwhile package)
    name='netools',
    url='https://bitbucket.org/justinlillico/neto_tools',
    author='Justin Lillico',
    author_email='justin.lillico@neto.com.au',
    # Needed to actually package something
    packages=['netools'],
    # Needed for dependencies
    install_requires=['pandas', 'sqlalchemy'],
    # *strongly* suggested for sharing
    version='0.2',
    # The license can be anything you like
    license='Neto',
    description='Backbone of Neto Python jobs.',
    # We will also need a readme eventually (there will be a warning)
    # long_description=open('README.txt').read(),
)