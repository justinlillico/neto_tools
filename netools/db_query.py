import pandas as pd

class NetoQuery(object):

    def quote_sql(self, string):
        # Try and parse it as an int, if it works just return it. If not, sql stringify it.
        try:
            int(string)
            return str(string)
        except:
            return "'" + str(string).replace("'", "''") + "'"

    def create_insert_statements(self, dataframe, schema, table_name, field_table):
            
        output_strings = []

        for index, row in dataframe.iterrows():
            output_start = "INSERT INTO " + schema + "." + table_name + " ("
            output_finish = " VALUES ("

            for key in field_table.keys():
                output_start += key + ", "
                output_finish += self.quote_sql(row[field_table[key]]) + ", "
              
            output_start = output_start[:-2] + ")"
            output_finish = output_finish[:-2] + ");"

            output_strings.append(output_start + output_finish)

        return output_strings

    def list_to_sql(self, filename, string_list):
        with open(filename, 'w') as f:
            for item in string_list:
                f.write("%s\n" % item)

    def convert_dates(self, dataframe):
        for col in dataframe.columns:
            if dataframe[col].dtype == 'object':
                try:
                    dataframe[col] = pd.to_datetime(dataframe[col])
                except ValueError:
                    pass
        return dataframe

    def update_table(self, schema, table_name, columns_values, condition_column, condition_value):
        output = f"UPDATE {schema}.{table_name} SET "
        for col in columns_values.keys():
            try:
                int(columns_values[col])
                output += f"{col} = {columns_values[col]}, "
            except ValueError:
                columns_values[col] = columns_values[col].replace("'", "''")
                output += f"{col} = '{columns_values[col]}', "
        
        output = output[:-2]
        output += f" WHERE {condition_column}={condition_value}"
        output = output.replace("\n", "")
        output = output.replace("\r", "")
        output += ";"
        
        return output