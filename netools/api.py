import petl
import requests
from json import loads, dumps

g_url = "https://$customer$/do/WS/NetoAPI"

categories_api_map = {

    "CategoryName" : "name",
    "ParentCategoryID" : "content path",
    "Description1" : "description 1",
    "Description2" : "description 2",
    "SEOMetaDescription" : "seo meta description"

}

headers = {

    "NETOAPI_KEY" : "",
    "NETOAPI_ACTION" : "",
    "Accept" : "application/json"

}


def petl_categories_to_api(table, customer_url, api_key, _map=categories_api_map):
    url = g_url.replace("$customer$", customer_url)

    headers["NETOAPI_KEY"] = api_key
    headers["NETOAPI_ACTION"] = "GetContent"

    name_to_id = {}

    # Add a field for parent count and direct parent.
    table = petl.addfield(table, "parent_count", lambda rec: len(rec[categories_api_map["ParentCategoryID"]].split(" > ")) if rec[categories_api_map["ParentCategoryID"]] != "" else 0)
    table = petl.addfield(table, "direct_parent", lambda rec: rec[categories_api_map["ParentCategoryID"]].split(" > ")[-1])

    # Sort table so that least parents first.
    table = petl.sort(table, "parent_count")

    # Iterate over and save each name and do a Get_Content to determine if it exists already and if so, store {name : id} in the dict.
    rows = petl.dicts(table)
    l = []

    for row in rows:
        content = row[categories_api_map["CategoryName"]]
        parent_name = row["direct_parent"]
        if content != "" and content not in l:
            l.append(content)

        if parent_name != "" and parent_name not in l:
            l.append(parent_name)

    r =  {"Filter" : {"OutputSelector" : ["ContentName", "ContentID"]}}
    r["Filter"]["ContentName"] = l
    
    response = requests.post(url, json=r, headers=headers)
    
    for element in loads(response.text)["Content"]:
        name_to_id[element["ContentName"]] = element["ContentID"]


    # Iterate over again and if the name is in the name_to_id, skip it. Otherwise, send it and store the returned {name : id} in the dict.
    # If the last category in the parent relationship field is in the name_to_id list, use the id in the send.

    insert_json = {"Content" : []}
    headers["NETOAPI_ACTION"] = "AddContent"

    for row in rows:
        name = row.get(categories_api_map["CategoryName"], "")
        parent_id = name_to_id.get(row.get("direct_parent", ""), "")
        desc1 = row.get(categories_api_map["Description1"], "")
        desc2 = row.get(categories_api_map["Description2"], "")
        seo = row.get(categories_api_map["SEOMetaDescription"], "")
        
        if name not in name_to_id.keys():
            insert_json["Content"] = [
                {
                    "ContentName" : name,
                    "ContentType" : "Product Category",
                    "ParentContentID" : parent_id,
                    "Description1" : desc1,
                    "Description2" : desc2,
                    "SEOMetaDescription" : seo

                }
            ]

            response = requests.post(url, json=insert_json, headers=headers)
            
            # This will get the one we have just added's ID and add it to the index of ID's
            if name not in name_to_id:
                _id = int(loads(response.text)["Content"]["ContentID"])
                name_to_id[name] = _id




if __name__ == "__main__":
    table = petl.fromcsv("category_import_template.csv")
    petl_categories_to_api(table, "brawndo.staging-aws.neto.net.au", "mAGuEfyATNoLj480eTxmAZhNsBPOUZtf")