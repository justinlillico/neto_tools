import multiprocessing
import pandas as pd
from database import *
import argparse
import numpy as np

def process_inserts(data):

    chunk = data[0]
    client_table = data[1]
    client_schema = data[2]
    my_number = data[3]

    columns = list(chunk.columns)

    f = open(client_schema + "_" + client_table + "_" + datetime.today().strftime('%Y%m%d') + "_" + str(my_number) + ".sql", "w", encoding="utf-8")

    for index, row in chunk.iterrows():
        output_start = "INSERT INTO " + client_schema + "." + client_table + " ("
        output_finish = " VALUES ("

        for key in columns:
            if row[key] is None:
                continue
            output_start += key + ", "
            output_finish += quote_sql(row[key]) + ", "
            
        output_start = output_start[:-2] + ")"
        output_finish = output_finish[:-2] + ");\n"

        f.write(output_start + output_finish)

    
def backup_table(client_schema, client_table, username, password):

    # create as many processes as there are CPUs on your machine
    num_processes = multiprocessing.cpu_count()

    engine = get_engine_for(DatabaseType.LIVE_MYSQL, client_schema, username, password)
    conn = get_ansi_connection(engine)

    df = pd.read_sql("SELECT * FROM " + client_schema + "." + client_table, conn)

    # calculate the chunk size as an integer
    # chunk_size = int(df.shape[0]/num_processes)

    # will work even if the length of the dataframe is not evenly divisible by num_processes
    chunks = np.array_split(df, num_processes) # [[df.iloc[df.index[i:i + chunk_size]], client_table, client_schema, i] for i in range(0, df.shape[0], chunk_size)]
    chunks = [[i_chunk[1], client_table, client_schema, i_chunk[0]] for i_chunk in enumerate(chunks)]


    # create our pool with `num_processes` processes
    pool = multiprocessing.Pool(num_processes)

    # apply our function to each chunk in the list
    pool.map(process_inserts, chunks)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Get a specific table from an SQL database dump.')
    parser.add_argument('schema', help='The schema in question.')
    parser.add_argument('table', help='The table in question.')
    parser.add_argument('username', help='Database username.')
    parser.add_argument('password', help='Database password.')

    args = parser.parse_args()

    backup_table(args.schema, args.table, args.username, args.password)
    