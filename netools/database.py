from enum import Enum
from sqlalchemy import create_engine
from os.path import join, expanduser
from datetime import datetime
import pandas as pd


class DatabaseType(Enum):
    MSSQL = 'mssql+pyodbc://localhost/$schema$?driver=SQL+Server+Native+Client+11.0?trusted_connection=yes'
    MYSQL = 'mysql://root:hB&=I?5*g5GA#L0gSOia@localhost/$schema$'
    LIVE_MYSQL = 'mysql://$username$:$password$@dbproxy.production.neto.net.au/$schema$'
    STAGING_MYSQL = 'mysql://$username$:$password$@dbproxy.staging-aws.neto.net.au/$schema$'


def get_engine_for(db:DatabaseType, schema, username=None, password=None):
    conn_string = db.value

    ssl_args = {}

    if db is DatabaseType.LIVE_MYSQL:
        if (not password or not username): 
            raise Exception("When connecting to live database, a username and password parametre must be provided.")
        else:
            conn_string = conn_string.replace("$password$", password).replace("$username$", username)
            ssl_args = {'ssl': join(expanduser("~"), "certificate.pem")}
    
    conn_string = conn_string.replace("$schema$", schema)

    return create_engine(conn_string, connect_args=ssl_args)

def get_ansi_connection(engine):
    connection = engine.connect()
    connection.execute("SET SESSION sql_mode=ANSI_QUOTES")
    return connection

def quote_sql(string):
        # Try and parse it as an int, if it works just return it. If not, sql stringify it.
        try:
            int(string)
            return str(string)
        except:
            if str(string).lower() == "null":
                return str(string)

            return "'" + str(string).replace("'", "''").replace('\r', '').replace('\n', '') + "'"


