import re
import argparse
from os.path import exists
# Open the sql file.

def main(tablename, filename, output):
    sql = open(filename, "r", encoding="utf-8").readlines()
    table_to_look_for = tablename
    table_to_look_for_reference = "`" + table_to_look_for + "`"
    reg = r"\/\*!(.....).*\*\/"

    if not output:
        output = "_TMP"
    output = open(table_to_look_for + output + ".sql", "w", encoding="utf-8")

    extract_line = False
    exists = False

    for line in sql:
        
        if "-- Table structure for table `" + table_to_look_for + "`" in line:
            extract_line = True
            exists = True
            continue
        elif not extract_line:
            continue

        # If we get here, we are currently parsing the table of interest.

        # This will rename the tables.
        if table_to_look_for_reference in line:
            line = line.replace(table_to_look_for_reference, table_to_look_for_reference[:-1] + "_TMP`")

        if "UNLOCK TABLES;" in line:
            output.write(line)
            break

        # Some exceptions.
        if "DROP TABLE IF EXISTS" in line:
            line = "\n"

        if bool(re.search(reg, line)) and '40000' not in line and '40101' not in line:
            line = "\n"

        if extract_line:
            output.write(line)

    output.close()
    if not exists:
        print("Table " + tablename + " not found in data.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Get a specific table from an SQL database dump.')
    parser.add_argument('table', help='The table in question.')
    parser.add_argument('-f', '--file', required=True, help='The sql dump file to parse')
    parser.add_argument('-o', '--output', help='The output filename. Default will be the table name + "_TMP"')

    args = parser.parse_args()

    # Errorchecking.
    if not exists(".\\" + args.file):
        raise Exception("Dump file does not exist in current directory.")

    main(args.table, args.file, args.output)