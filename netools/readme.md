# Netools

## Introduction
Netools is a Python library for working on common Neto tasks. It is built on top of the pandas library for performing transformations on datasets. It used SQLAlchemy to communicate with different databases.

Please note that there should only be raw passwords here for local SQL instances where security is not an issue. There should **not** be any live database passwords kept in here.

## Requirements
SQLAlchemy
Pandas

## Optional
locationiq (for processing latitude and longitude requirements).

## Installation
It is recommended that you install this package globally for ease of use but you may use it in virtual environments if you like.

`pip install git+https://justinlillico@bitbucket.org/justinlillico/neto_tools.git`

Please note you will need the correct permissions to install this.

## Modules

### database
This module contains everything you need to connect to different databases.

### db_query
This module contains the **NetoQuery** object which is used to perform various different useful functions for processing data.

### extract_table
This is a command line tool that you can pass an SQL dump file to and specify a table for it to extract and it will produce a single SQL file to create the one table.

### store_locations
This module is for taking a pandas dataframe and appending a latitude and longitude column for the locations of addresses.

## Notes
This library is still very much in devlopment. Most of the functionality is designed for use only by myself so there is likely going to be issues if anyone attempts to use it elsewhere. However, I am more than open to feedback and suggestions. 

You may contact me on Slack or at justin.lillico@neto.com.au

Thanks!