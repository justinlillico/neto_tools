import extract_table, backup_table
from multiprocessing import freeze_support
import argparse



restores = {
    "inventory" : [

        "INVENTORY", 
        "CONTENTS", 
        "MANAGED_SALES_CHANNEL", 
        "SITE_URL",
        "URL_INFO",
        "INVENTORY_BUNDLES",
        "INVENTORY_BUNDLES_GROUP",
        "INVENTORY_CARTONS",
        "INVENTORY_COLUMNS",
        "INVENTORY_CONTENTS",
        "IMAGES",
        "INVENTORY_IMAGES",
        "INVENTORY_MANAGED_SALES_CHANNEL",
        "INVENTORY_MOQS",
        "INVENTORY_MULTILEVELPRICES",
        "INVENTORY_PRICES",
        "INVENTORY_QTY_LOGS",
        "INVENTORY_RECORDS",
        "INVENTORY_SPECIFICS",
        "INVENTORY_WAREHOUSES"

    ]
}


def main(restore_type, n_number, _file, db_username, db_password):
    for table in restores[restore_type]:
        backup_table.backup_table(n_number + "_main", table, db_username, db_password)
        extract_table.main(table, _file, "_to_insert")
        with open(table + "_to_insert.sql", "r") as f:
            lines = f.readlines()

        new_lines = []
        for line in lines:
            if "INSERT INTO" in line:
                new_lines.append(line.replace("INSERT INTO", "INSERT IGNORE INTO").replace("_TMP` ", "` "))

        with open(table + "_to_insert.sql", "w") as fi:
            fi.writelines(new_lines)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Will break apart a dump file into the necessary inserts as well as take a backup of all required tables.')
    parser.add_argument('restore_type', choices=list(restores.keys()), help='The type of restore to do.')
    parser.add_argument('n_number', help='The clients n number (no _main)')
    parser.add_argument('file', help='The sql dump file to parse')
    parser.add_argument('db_username', help='Database username.')
    parser.add_argument('db_password', help='Databse password.')

    args = parser.parse_args()

    freeze_support()

    main(args.restore_type, args.n_number, args.file, args.db_username, args.db_password)

    
