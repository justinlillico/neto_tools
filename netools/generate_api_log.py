import database
import argparse
import pandas as pd



def main(client, api):
    engine = database.get_engine_for(database.DatabaseType.LIVE_MYSQL, client + "_main", username=input("Please enter username: "), password=input("Please enter password: "))
    conn = database.get_ansi_connection(engine)

    api_map = {

        'paypal' : "processor_module='paypal_express' OR processor_module='paypal'",
        'stripe' : "processor_module='stripe'",
        'eway' : "processor_module='eway_securepanel' OR processor_module='eway_rapid'"

    }

    df = pd.read_sql("SELECT * FROM INTEGRATION_API_LOG WHERE " + api_map[api], conn).fillna("")

    df.to_excel(client + "_" + api + "_Log.xlsx", index=False)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Get an api log.')
    parser.add_argument('client', help='The N number of the client to get.')
    parser.add_argument('api', help='The api you would like logs for.')
    
    args = parser.parse_args()
    main(args.client, args.api)
