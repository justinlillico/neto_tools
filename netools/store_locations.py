import pandas as pd
from locationiq.geocoder import LocationIQ, LocationIqNoPlacesFound

# This module will take a given dataframe along with a list of address fields to use. It will return the given dataframe with the latitude and longitude included.

def get_latlon(df, address_fields):

    # Connect to API.

    api_key = "pk.f2122962945afd65d3ea7c14167f9854"

    geocoder = LocationIQ(api_key)

    for index, row in df.iterrows():

        print(str(index) + " of " + str(len(df.index)) + " processed...")
        address = ""

        for add in address_fields:
            address += str(row[add]) + " "

        add = add[:-1]

        if address:
            passed = False

            while not passed:

                if not address:
                    break

                try:
                    search = geocoder.geocode(address)
                    passed = True
                except:
                    print("Address: " + address)
                    address = input("Try something else or leave blank to skip: ")

            if address:
                df.at[index, 'stloc_lat'] = float(search[0]['lat'])
                df.at[index, 'stloc_lng'] = float(search[0]['lon'])
    